import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { library } from '@fortawesome/fontawesome-svg-core';
import { faStar, faSignInAlt } from '@fortawesome/free-solid-svg-icons';
import { faLock } from '@fortawesome/free-solid-svg-icons';
import { CookieService } from 'ngx-cookie-service';
import { HttpClientModule } from '@angular/common/http';

library.add(faStar);
library.add(faLock);
library.add(faSignInAlt);

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule, 
    FontAwesomeModule

  ],
  providers: [CookieService],
  bootstrap: [AppComponent]
})
export class AppModule { }
