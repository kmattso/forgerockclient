export interface IAuthRequest {

    tokenId: String;
    successUrl: String;
    realm: String; 
}
export interface IEntitlementRequest {
    resources: string[];
    application: string;
}


export interface IEntitlements {

    resource: string;
    action: IActions;
    attribute: IAttribute;
    advice: IAdvice;
}

export interface IActions {
    View: boolean;
}

export interface IAttribute {
    Response: string[];
}
export interface IAdvice {
    Value: string;
}