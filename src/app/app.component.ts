import { Component } from '@angular/core';
import { AuthzService } from './authz.service';
import { Configuration } from './app.constants';
import { Auth } from './auth'
import { IAuthRequest, IEntitlementRequest, IEntitlements } from './interfaces';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [AuthzService, Configuration]
})
export class AppComponent {
  title = 'forgerock-client.......';

  constructor(private authzService: AuthzService) {}

  auth: IAuthRequest;
  errorMessage: string;
  entitlements: IEntitlements


  showSsn(componentid: string) {
    console.log('check priv on ssn:' + componentid);
    var entitlementreq = <IEntitlementRequest> { application: 'Homescreen', resources: ['tiles']}

    this.authzService.getEntitlement(entitlementreq).subscribe(
      (data) => {
        this.entitlements = data;
      },
      () => {
        this.errorMessage = "Auth not working";
      });

  }


  login(user: string) {
    console.log('logging in...' );
    this.authzService.login(user).subscribe(
      (data) => {
        this.auth = data;
      },
      () => {
        this.errorMessage = "Auth not working";
      });
  }
}
