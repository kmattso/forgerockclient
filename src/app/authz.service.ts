import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of} from 'rxjs';
import { Configuration } from './app.constants';
import { map, catchError, tap } from 'rxjs/operators';
import { CookieService } from 'ngx-cookie-service';
import { IEntitlementRequest } from './interfaces';
import { IEntitlements } from './interfaces';



const httpOptions = {
    headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'my-auth-token',
    //    'X-OpenAM-Username': 'swprofessional',
    //    'X-OpenAm-Password': 'swprofessional',
        'Accept-API-Version': 'resource=2.0, protocol=1.0'

    })
};

@Injectable()
export class AuthzService {


    constructor(private http: HttpClient,
                private configuration: Configuration,
                private cookieService: CookieService) {

    }

    private extractData(res: Response) {
        let body = res;
        return body || {};
    }


    public login(user: string): Observable<any> {
        console.log('in the login()');
        let loginHeader = new HttpHeaders({'Content-Type': 'application/json',
        'Authorization': 'my-auth-token',
        'X-OpenAM-Username': 'swprofessional',
        'X-OpenAm-Password': 'swprofessional',
        'Accept-API-Version': 'resource=2.0, protocol=1.0'});

      //  httpOptions.headers.append("X-OpenAM-Username", user);
      //  httpOptions.headers.append("X-OpenAM-Password", user);
        return this.http.post<any>
            (this.configuration.endpoint + '/authenticate', '' , loginHeader)
                .pipe(map(this.extractData),
            catchError(this.handleError<any>('login'))
        );

    }

    getEntitlement(entitlementrequest: IEntitlementRequest): Observable<any> {

        console.log('in the getEntitlement()' );

        return this.http.post<any>(this.configuration.endpoint + '/policies?_action=evaluate', entitlementrequest, httpOptions)
            .pipe(map(this.extractData),
            catchError(this.handleError<any>('getEntitlement'))
        );

    }


    public getAll<T>(): Observable<T> {
        return this.http.get<T>(this.configuration.endpoint);
    }

    private handleError<T>(operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {
            console.error(error); // log to console instead
            console.log(`${operation} failed: ${error.message}`);
            return of(result as T);
        };
    }


}
