import { Injectable } from '@angular/core';

@Injectable()
export class Configuration {
    public endpoint = 'http://localhost:8080/openam/json/realms/root/realms/serviceworks';

}